# My project's README

#Overview
This is a RESTful web service developed based on JAX-RS.

		Database : SQLite 3.14


		RESTFul API's to manage company products : JAX-RS


		Front End : HTML, CSS, JQuery 3.2.1

#API Endpoints:

GET http://localhost:8080/ci-app/rest/products - This end point enables you to get a full list of products.



GET http://localhost:8080/ci-app/rest/product/{id} - This end point enables you to get one product by ID. In this end point id is a variable for the product and can be any number.



POST http://localhost:8080/ci-app/rest/product/product � This end point enables you to add one product. You need to pass title, price and description as a json similar to following. 
			

    {  
    	"Title":"test",
    	"Description":"test",
    	"Price":1000
    }

PUT http://localhost:8080/ci-app/rest/product/{id} � This end point enables you to update one product by given id. You need to pass title, price and description as a json.
			

    {  
        "Title":"test",
        "Description":"test",
        "Price":1000
    }

DELETE http://localhost:8080/ci-app/rest/product/{id} � This end point enables you to delete one product by given id.



#Project Setup:


- Import the CareerInteractive.sql database dump which will create the database and the table.

- Change the database connection url in /app/ci-app/src/main/java/org/ci/app/dao/DbConnection.java file to point to your database

- Build and Run the web app either using terminal or an IDE with Maven.

- To test the backend functionalities you can make API requests using a tool like Postman

- Front end URL : http://localhost:8080/ci-app/
		

#To Do:


Handling exceptions better 

Writing back meaningful responses

Front end - Error handling, Improvements to UI

 

