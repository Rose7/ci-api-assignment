/**
 * This is the jquery for crud operations
 */
// Base URL to call services
var baseURL = "http://localhost:8080/ci-app/rest/";

//Document ready function to load navigation bar and to load all products
$(document).ready(function () {
    $('.nav ul li:first').addClass('active');
    $('.tab-content:not(:first)').hide();
    $('.nav ul li a').click(function (event) {
        event.preventDefault();
        var content = $(this).attr('href');
        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');
        $(content).show();
        $(content).siblings('.tab-content').hide();
    });
    //loading all products    
    $.getJSON("http://localhost:8080/ci-app/rest/products",
    	    function (json) {
    	        var tr;
    	        for (var i = 0; i < json.length; i++) {
    	            tr = $('<tr/>');
    	            tr.append("<td>" + json[i].ID + "</td>");
    	            tr.append("<td>" + json[i].Title + "</td>");
    	            tr.append("<td>" + json[i].Price + "</td>");
    	            $('table').append(tr);
    	        }
    	    });
});



$('#btnDelete').click(function() {
	var delid = $('#searchId').val();
	console.log(delid)
	deleteProduct(delid);
	return false;

});

$('#btnAdd').click(function() {
	addProduct();
	return false;
});

$('#btnUpdate').click(function() {
	var updateid = $('#searchId').val();
	console.log(updateid)
	updateProduct(updateid);
	return false;
});


//Add product
function addProduct() {
	console.log('adding product');
	$.ajax({
		async: false,
		type: 'POST',
		contentType: 'application/json',
		url: baseURL + 'product',
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('Product created successfully');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error in adding product: ' + textStatus);
		}
	});
}

//Update product 
function updateProduct(id) {
	console.log('Updating product');
	$.ajax({
		async: false,
		type: 'PUT',
		contentType: 'application/json',
		url: baseURL + 'product/' + id,
		dataType: "json",
		data: updateFormToJSON(),
		success: function(data, textStatus, jqXHR){
			alert(' updated successfully');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('update error: ' + textStatus);
		}
	});
}


//Delete product
function deleteProduct(id) {
	console.log('deletingProduct');
	$.ajax({
		type: 'DELETE',
		url: baseURL + 'product/' + id,
		success: function(data, textStatus, jqXHR){
			alert('Product deleted successfully');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error deleting product');
		}
	});
}


//Get product for a given id
$('#btnSearch').click(function() {
	var pid = $('#searchId').val();
	getProduct(pid);
	return false;
});

// Search for a product
function getProduct(id) {
	console.log('findById: ' + id);
	$.ajax({
		//async: false,
		type: 'GET',
		url: baseURL + 'product/'+ id,
		dataType: "json",
		success: function(data){
			console.log('getProduct success: ' + data.ID);
			retrievedProduct = data;
			renderDetails(retrievedProduct);

		}
	});	
}

//Rendering the product details
function renderDetails(product) {
	$('#productId').val(product.ID);
	$('#gettitle').val(product.Title);
	$('#getdescription').val(product.Description);
	$('#getprice').val(product.Price);
	$('#getcreated_date').val(product.Created_at);
	$('#getupdated_date').val(product.Updated_at);
}


// Convert all the form fields into a JSON string
function formToJSON() {
		return JSON.stringify({
		"Title": $('#title').val(), 
		"Description": $('#description').val(),
		"Price": $('#price').val(),
		});
}

function updateFormToJSON() {
	return JSON.stringify({
	"Title": $('#gettitle').val(), 
	"Description": $('#getdescription').val(),
	"Price": $('#getprice').val(),
	});
}
