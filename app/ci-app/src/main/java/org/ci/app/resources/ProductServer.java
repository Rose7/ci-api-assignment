package org.ci.app.resources;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.ci.app.dao.DaoImpl;
import org.ci.app.model.Product;

@Path("/product")
public class ProductServer {
	DaoImpl productDaoImpl = new DaoImpl();

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Product getProduct(@PathParam("id") int id) {
		System.out.println(id);
		return productDaoImpl.getProduct(id);
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteProduct(@PathParam("id") int id) throws Exception{
		System.out.println("id to remove: " + id);
		if(productDaoImpl.deleteProduct(id)){
		return Response.status(Status.OK).build();
		}
		return null;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public Product addProduct(Product p) throws SQLException{
		System.out.println("Title: " + p.getTitle());
		System.out.println("Description: " + p.getDescription());
		System.out.println("Price: " + p.getPrice());
		return productDaoImpl.addProduct(p);
		//return Response.status(Status.CREATED).entity(p.getID()).build();
	}
	
	@PUT
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public void updateProduct(Product p, @PathParam("id") int id) throws SQLException{
		System.out.println("Title: " + p.getTitle());
		System.out.println("Description: " + p.getDescription());
		System.out.println("Price: " + p.getPrice());
		productDaoImpl.updateProduct(p, id);
	}
}
