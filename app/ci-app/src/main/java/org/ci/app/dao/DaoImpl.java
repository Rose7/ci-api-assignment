package org.ci.app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.ci.app.model.Product;

public class DaoImpl {
	
		public static List<Product> getProductDetails() {
		
		List<Product> productDetails = new ArrayList<>();
		
		DbConnection dbConnection = new DbConnection();
		Connection connection = dbConnection.getConnnection();

		System.out.println("connection successful");
		try {
		 PreparedStatement ps = connection.prepareStatement(
		 "select * from Products");
		 ResultSet rs = ps.executeQuery();
		 
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 
		while (rs.next()) {
		 Product product = new Product();
		 product.setID(rs.getInt("ID"));
		 product.setTitle(rs.getString("Title"));
		 product.setDescription(rs.getString("Description"));
		 product.setPrice(rs.getDouble("Price"));
		 try {
			product.setCreated_at(sdf.parse(rs.getString("Created_at")));
		} catch (ParseException e) {
			System.out.println(e.getClass().getName() + ": " +e.getMessage());
		}
		 try {
				product.setUpdated_at(sdf.parse(rs.getString("Updated_at")));
			} catch (ParseException e) {
				System.out.println(e.getClass().getName() + ": " +e.getMessage());
			}
		 productDetails.add(product);
		}
		System.out.println("recordset created");
		} catch (SQLException e) {
		 
			System.out.println(e.getClass().getName() + ": " +e.getMessage());
		 }

		return productDetails;
		 }
	
	//Retrieving a single product
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Product getProduct(int id) {		
		Product product = new Product();
		DbConnection dbConnection = new DbConnection();
		Connection connection = dbConnection.getConnnection();
		try {
			String query = "select * from Products where ID = ?";
			 PreparedStatement ps = connection.prepareStatement(query);
			 ps.setInt(1, id);
			 System.out.println("id " + id);
			 ResultSet rs = ps.executeQuery();
			 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			 if (rs.next()) {				 
				 product.setID(rs.getInt("ID"));
				 product.setTitle(rs.getString("Title"));
				 System.out.println(rs.getString("Description"));
				 product.setDescription(rs.getString("Description"));
				 product.setPrice(rs.getDouble("Price"));
				 try {
					product.setCreated_at(sdf.parse(rs.getString("Created_at")));
				 } catch (ParseException e) {
					System.out.println(e.getClass().getName() + ": " +e.getMessage());
				 }
				 try{
						product.setUpdated_at(sdf.parse(rs.getString("Updated_at")));
				 } catch (ParseException e) {
						System.out.println(e.getClass().getName() + ": " +e.getMessage());
				 }
		             
		        }
		}catch(SQLException e){
			System.out.println(e.getClass().getName() + ": " +e.getMessage());
		}		
		return product;
		
	}
	
	//Delete product
	public boolean deleteProduct(int id) throws SQLException {
        String query = "delete from Products where ID = ?";         
        DbConnection dbConnection = new DbConnection();
		Connection connection = dbConnection.getConnnection();		
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, id);         
        boolean deleted = ps.executeUpdate() > 0;
        System.out.println("record " + id + " deleted");
        ps.close();        
        return deleted;     
    }
	
	//Add product
	public Product addProduct(Product product) throws SQLException{
        String query = "insert into Products (title, description, price) VALUES (?, ?, ?)";
        DbConnection dbConnection = new DbConnection();
		Connection connection = dbConnection.getConnnection();         
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setString(1, product.getTitle());
        ps.setString(2, product.getDescription());
        ps.setDouble(3, product.getPrice());         
        //boolean created = ps.executeUpdate() > 0;
        ResultSet rs = ps.getGeneratedKeys();
        rs.next();
        int id = rs.getInt(1);
        product.setID(id);
        System.out.println("product id " + id);
        System.out.println("Created product successfully");
        ps.close();
        return product;
    }
	
	//Update product
	public boolean updateProduct(Product product, int id) throws SQLException{
        String query = "update Products set title=?, description=?, price=?";        
        query += "where id = ?";
        DbConnection dbConnection = new DbConnection();
		Connection connection = dbConnection.getConnnection();         
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setString(1, product.getTitle());
        ps.setString(2, product.getDescription());
        ps.setDouble(3, product.getPrice());
        ps.setInt(4, id);
        boolean updated = ps.executeUpdate() > 0;
        System.out.println("Updated product successfully");
        ps.close();
        return updated;
    }

}
