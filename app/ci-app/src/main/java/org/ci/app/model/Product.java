package org.ci.app.model;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;

@XmlRootElement(name = "Product")
@XmlAccessorType(XmlAccessType.FIELD)

public class Product {
	private String Title;
    private String Description;
    private Double Price;
    private int ID;
    Date Created_at;
    Date Updated_at;
     
    // Default constructor
    public Product() { 
    }
 
    public Product(String Title, String Description, Double Price, int ID) {
        this.Title = Title;
        this.Description = Description;
        this.Price = Price;
        this.ID = ID;
    }
 
    public String getTitle() {
        return Title;
    }
 
    public void setTitle(String Title) {
        this.Title = Title;
    }
 
    public String getDescription() {
        return Description;
    }
 
    public void setDescription(String Description) {
        this.Description = Description;
    }
 
    public Double getPrice() {
        return Price;
    }
    
    public void setPrice(Double Price) {
        this.Price = Price;
    }
    
    public void setCreated_at(Date Created_at) {
        this.Created_at = Created_at;
    }
    
    public Date getCreated_at(){
    	return Created_at;
    }
    
    public void setUpdated_at(Date Updated_at) {
        this.Updated_at = Updated_at;
    }
    
    public Date getUpdated_at(){
    	return Updated_at;
    }
    
    public int getID() {
        return ID;
    }
    
    public void setID(int ID) {
        this.ID = ID;
    }
}
