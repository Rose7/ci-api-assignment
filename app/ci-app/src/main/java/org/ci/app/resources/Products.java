package org.ci.app.resources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.ci.app.dao.DaoImpl;
import org.ci.app.model.Product;


@Path("/products")

public class Products {		
	DaoImpl productDaoImpl = new DaoImpl();
	List<Product> productList = DaoImpl.getProductDetails();
		
	@GET
	@Produces(MediaType.APPLICATION_JSON)	
	public List<Product> getProducts(){
		return productList;				
	}

}
