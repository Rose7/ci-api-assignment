package org.ci.app.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbConnection {
	
	public Connection getConnnection() {
	 Connection connection = null;

	try {
		System.out.println("Trying to connect to SQLite database");
		Class.forName("org.sqlite.JDBC");
		String connectionURL = "jdbc:sqlite:/Users/harangala/stash_projects/ci-api-assignment/sqlite/CareerInteractive.sqlite";
	 
	 connection = DriverManager.getConnection(connectionURL);
	 System.out.println("Connection to SQLite has been established.");

	} catch (Exception e) {
	 System.out.println(e.getClass().getName() + ": " +e.getMessage());
	 }
	 return connection;
	 }
}



